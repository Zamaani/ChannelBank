<!DOCTYPE HTML>
<!--
	Justice by freehtml5.co
	Twitter: http://twitter.com/fh5co
	URL: http://freehtml5.co
-->
<html class="dre">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ممبران</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Free HTML5 Website Template by FreeHTML5.co" />
    <meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
    <meta name="author" content="FreeHTML5.co" />

    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:description" content=""/>
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />

    <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400, 900" rel="stylesheet"> -->

    <!-- Animate.css -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/fontiran.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/mystyle.css">

    <!-- Modernizr JS -->
    <script src="js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<div class="gtco-loader"></div>

<div id="page" style="overflow: hidden;">
    <nav class="gtco-nav" role="navigation">
        <div class="container">
            <div class="row">

                <div class="col-xs-10 text-right menu-1 main-nav">
                    <ul>
                        <li class="fltn fltr"><a href="#" data-nav-section="about">درباره ما</a></li>
                        <li class="fltn fltr"><a href="#" data-nav-section="our-team">اهمیت</a></li>
                        <li class="fltn fltr"><a href="#" data-nav-section="practice-areas">امکانات</a></li>
                        <li><a href="#" data-nav-section="contact">پاورقی</a></li>
                        <li class="fltn" style="float: left;"><a href="#" >ورود</a></li>
                        <!-- For external page link -->
                        <!-- <li><a href="http://freehtml5.co/" class="external">External</a></li> -->
                    </ul>
                </div>

                <div class="col-sm-2 col-xs-12">
                    <div id="gtco-logo"><a href="#" data-nav-section="home">ممبران</a></div>
                </div>

            </div>

        </div>
    </nav>

    <section id="gtco-hero" class="gtco-cover" style="background-image: url(images/img_bg_4.jpg);"  data-section="home"  data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-md-offset-0 text-center">
                    <div class="display-t">
                        <div class="display-tc">
                            <h1 class="animate-box" data-animate-effect="fadeIn">با ممبران <b style="color: #1abc9c;font-weight: bolder!important;">بهترین نتیجه</b> را بگیرید</h1>
                            <!-- <hr style="border: 2px solid #95a592;max-width: 70%;"> -->
                            <a href="/signin/demo" class="mybtn mybtn-warning mybtn-lg">ثبت سفارش</a>
                            <a href="/signup" class="mybtn mybtn-primary mybtn-lg">دانلود نرافزار</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="gtco-about" data-section="about">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-pull-1 animate-box" data-animate-effect="fadeInLeft">
                    <div class="img-shadow">
                        <img src="images/img_1.jpg" class="img-responsive" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
                    </div>
                </div>
                <div class="col-md-6 animate-box" data-animate-effect="fadeInLeft">
                    <h1 class="heading-colored">ممبران چیست؟</h1>
                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.</p>
                    <p><a href="#" class="read-more"><i class="icon-chevron-left"></i>ادامه مطلب</a></p>
                </div>
            </div>
        </div>
    </section>

    <section class="imgback" data-section="our-team">
        <div class="rowback" id="mygtco-our-team">

            <div class="row team-item gtco-team">
                <div class="container">
                    <div class="col-md-6 animate-box"  data-animate-effect="fadeInLeft" style="float: right;">
                        <div style="border-right: 6px solid #1abc9c;padding-right: 50px;">
                            <p style="font-weight: normal; font-size: 50px; color: #fff;">با <b style="color: #1abc9c;">ممبران</b> همیشه <br> برترین باشید</b>
                            <p style="font-weight: normal; font-size: 30px; color: #fff;">با ما همراه شوید تا قله موفقیت</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <section id="gtco-practice-areas" data-section="practice-areas">
        <div class="container">
            <div class="row">

                <div class="col-md-6 fltr">

                    <div class="gtco-practice-area-item animate-box">
                        <div class="gtco-icon">
                            <img src="images/scale.png" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
                        </div>
                        <div class="gtco-copy">
                            <h3 style="font-size: 50px;">امکانات ممبران:</h3>
                            <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط. </p>
                        </div>
                    </div>

                </div>

                <div class="col-md-6">
                    <div class="gtco-practice-area-item animate-box">
                        <div class="gtco-icon">
                            <img src="images/scale.png" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
                        </div>
                        <div class="gtco-copy">
                            <h3>امکانات ممبران</h3>
                            <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط. </p>
                        </div>
                    </div>

                    <div class="gtco-practice-area-item animate-box">
                        <div class="gtco-icon">
                            <img src="images/scale.png" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
                        </div>
                        <div class="gtco-copy">
                            <h3>امکانات ممبران</h3>
                            <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط. </p>
                        </div>
                    </div>

                    <div class="gtco-practice-area-item animate-box">
                        <div class="gtco-icon">
                            <img src="images/scale.png" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
                        </div>
                        <div class="gtco-copy">
                            <h3>امکانات ممبران</h3>
                            <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط. </p>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>


    <section class="index-link" style="padding: 3em;" data-section="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-3 fltr fltn">
                    <div class="link-area animate-box" data-animate-effect="fadeIn">
                        <h3 class="txtr">ممبران</h3>
                        <P style="color: #fff;">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط.</P>
                        <li class="fa-li"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li class="fa-li"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li class="fa-li"><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    </div>
                </div>
                <div class="col-md-3 fltr fltn">
                    <div class="link-area animate-box" data-animate-effect="fadeIn">
                        <h3 class="txtr">صفحات مرتبط</h3>
                        <ul>
                            <li><a href="#"> سوالات متداول</a></li>
                            <li><a href="#"> سوالات متداول1</a></li>
                            <li><a href="#"> سوالات متداول</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 fltr fltn">
                    <div class="link-area animate-box" data-animate-effect="fadeIn">
                        <h3 class="txtr">تماس با ما</h3>
                        <ul>
                            <li><a href="#"> ممبران</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="link-area animate-box" data-animate-effect="fadeIn">
                        <h3 class="txtr">لوگو</h3>
                        <ul>
                            <li><a href="#"> ممبران</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="index-social" style="height: 59px;">
        <div class="container">
            <div class="row index-social-link text-center">
                <p class="copy-c"> تمامی حقوق برای وبسایت ممبران محفوظ می باشد. 1395</p>
            </div>
        </div>
    </section>

</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>

<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="js/jquery.waypoints.min.js"></script>
<!-- Stellar -->
<script src="js/jquery.stellar.min.js"></script>
<!-- Magnific Popup -->
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/magnific-popup-options.js"></script>
<!-- Main -->
<script src="js/main.js"></script>

</body>
</html>

