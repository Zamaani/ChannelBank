<!DOCTYPE html>
<html>
<head>
    <title>ممبران</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/login.css">
    <link rel="stylesheet" type="text/css" href="css/fontiran.css">

</head>
<body>

<div class="container">
    <h1 class="welcome text-center">&nbsp <br>&nbsp </h1>
    <div class="card card-container">
        <h2 class='login_title text-center'>ورود</h2>
        <hr>

        <form class="form-signin">
            <span id="reauth-email" class="reauth-email"></span>
            <p class="input_title">ایمیل</p>
            <input type="text" id="inputEmail" class="login_box" placeholder="user01@IceCode.com" required autofocus>
            <p class="input_title">رمز عبور</p>
            <input type="password" id="inputPassword" class="login_box" placeholder="******" required>
            <div id="remember" class="checkbox">
                <label>

                </label>
            </div>
            <button class="btn btn-lg " type="submit">ثبت</button>
        </form><!-- /form -->
    </div><!-- /card-container -->
</div><!-- /container -->


</body>
</html>